package org.example;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static AtomicInteger counter = new AtomicInteger(0);
    public static final String numbers = "1234567890";
    public static final String operators = "+-*/!()";
    public static final String FILE_PATH = "/home/ubuntu/Documents/Telegram Desktop";
    public static int indx;
    public Double min = 0.0;
    public Double max = 0.0;
    public volatile Double sum = 0.0;
    InputStream in = new FileInputStream(FILE_PATH + File.separator + "question.txt");
    InputStreamReader r = new InputStreamReader(in);
    public final BufferedReader reader = new BufferedReader(r);

    public Main() throws FileNotFoundException {
    }


    public static void main(String[] args) throws Exception {
        Main m = new Main();
        Runnable runnable = () -> {
            try {
                m.process();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };

        Long begin = System.currentTimeMillis();
        Thread thread = new Thread(runnable);
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);

        thread.start();
        thread.join();
        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();

//        m.process();
        Long end = System.currentTimeMillis();

        System.out.println("Counter : " + counter);
        System.out.println("Sum : " + m.sum);
        System.out.println("Max : " + m.max);
        System.out.println("Min : " + m.min);
        System.out.println("Time : " + (end - begin));


    }

    public void process() throws Exception {
        while (reader.ready()) {
            String expression = reader.readLine();
            Double result = processExpression(expression);
            sum += (result);
            if (min > result)
                min = result;
            if (max < result)
                max = result;
        }


    }

    Double processExpression(String expression) {
        if (expression.contains("!")) {
            StringBuilder number = new StringBuilder();
            List<String> exp = new LinkedList<>();
            for (int i = 0; i < expression.length() + 1; i++) {
                if (i == expression.length()) {
                    exp.add(number.toString());
                    break;
                }
                if (numbers.contains(String.valueOf(expression.charAt(i)))) {
                    number.append(expression.charAt(i));
                } else if (operators.contains(String.valueOf(expression.charAt(i)))) {
                    exp.add(number.toString());
                    exp.add(String.valueOf(expression.charAt(i)));
                    number.delete(0, number.length());
                }
            }
            while (exp.contains("!")) {
                Integer x = fac(exp);
                if (x != -1) {
                    exp.add(indx, String.valueOf(x));
                }
            }
            StringBuilder updExp = new StringBuilder();
            for (String a : exp) {
                updExp.append(a);
            }
            Expression finalExp = new ExpressionBuilder(updExp.toString())
                    .build();
            counter.incrementAndGet();
            return finalExp.evaluate();

        } else {
            counter.incrementAndGet();
            Expression finalExp = new ExpressionBuilder(expression)
                    .build();
            return finalExp.evaluate();

        }

    }

    Integer fac(List<String> exp) {
        StringBuilder prev = new StringBuilder();
        for (String x : exp) {
            if (x.equals("!")) {
                indx = exp.indexOf("!") - 1;
                exp.remove(indx);
                exp.remove("!");
                return factorial(Integer.parseInt(prev.toString()));
            } else {
                prev.delete(0, prev.length());
                prev.append(x);
            }

        }
        return -1;
    }

    Integer factorial(int n) {
        if (n == 0)
            return 1;
        else
            return (n * factorial(n - 1));
    }


}